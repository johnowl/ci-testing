#!/bin/bash

function install_prerequisites() {
    apt-get update
    
    apt-get install -y python3.8
    apt-get install -y python3-pip
    apt-get install -y mongodb-clients
    apt-get install -y git

    pip3 install --upgrade python-gitlab    
    pip3 install --upgrade cookiecutter
}

function show_project_name() {
    echo "Seu projeto será criado com o nome $MY_PROJECT_NAME"
}

function download_template() {
    git clone --depth 1 -b master https://$GIT_USER:$GIT_TOKEN@gitlab.com/johnowl/ci-template.git template
}

function test_tools() {
    gitlab --version
    mongo --version
    cookiecutter --version
}

function apply_template() {
    cookiecutter --no-input template/ project_name=$MY_PROJECT_NAME
}

function create_gitlab_config() {
    sed 's/{{private_token}}/'"$GIT_TOKEN"'/g' python-gitlab.cfg.template > ~/.python-gitlab.cfg
}

function create_gitlab_project() {

    gitlab project create --name $MY_PROJECT_NAME \
         --only-allow-merge-if-pipeline-succeeds true \
         --only-allow-merge-if-all-discussions-are-resolved true \
         --request-access-enabled false > project-details

    sed -i -e 's/ //g' project-details  # remove espaços em branco
    sed -i -e 's/:/=/g' project-details # troca o : por =
    sed -i -e 's/^/export new_project_/' project-details # adiciona o prefixo expor new_project_ em cada linha
    source project-details

}

function send_initial_code_to_gitlab() {

    git config --global user.email $GIT_USER
    git config --global user.name $GIT_USER

    cd $MY_PROJECT_NAME
    git init
    git remote add origin https://$GIT_USER:$GIT_TOKEN@gitlab.com/johnowl/$MY_PROJECT_NAME.git
    git fetch
    git checkout -t origin/develop
    git add -A
    git commit -m "Initial commit."
    git push -u origin develop
    cd ..
}

function create_branches() {
    echo "Criando branch staging baseado na master"
    gitlab project-branch create --project-id $new_project_id --branch staging --ref master
    echo "Criando branch staging develop na staging"
    gitlab project-branch create --project-id $new_project_id --branch develop --ref staging
}

function protect_branches() {

    echo "Protegendo branch master"
    gitlab project-branch protect --project-id $new_project_id --name master \
         --developers-can-push false \
         --developers-can-merge true

    echo "Protegendo branch staging"
    gitlab project-branch protect --project-id $new_project_id --name staging \
         --developers-can-push false \
         --developers-can-merge true    

    echo "Protegendo branch develop"
    gitlab project-branch protect --project-id $new_project_id --name develop \
         --developers-can-push false \
         --developers-can-merge true    
}

function set_default_project_branch() {
    gitlab project update --id $new_project_id --default-branch develop
}

function associate_runner() {
    gitlab project-runner create --project-id $new_project_id \
     --runner-id $MY_RUNNER_ID
}

function create_mongodb_user() {

    cp $PUBLIC_KEY public_key.pem

    NEW_PASS=$(LC_ALL=C tr -dc 'A-Za-z0-9' </dev/urandom | head -c 64 ; echo)
    NEW_USER=$(echo "${MY_PROJECT_NAME}_user" | tr '[:upper:]' '[:lower:]')

    NEW_PASS_PROTECTED=$(echo $NEW_PASS | openssl rsautl -encrypt -pubin -inkey public_key.pem | base64)

    cp mongo.js.template mongo.js
    sed -i -e 's/{DATABASE_NAME}/'"$MY_PROJECT_NAME"'/g' mongo.js
    sed -i -e 's/{USERNAME}/'"$NEW_USER"'/g' mongo.js
    sed -i -e 's/{PASSWORD}/'"$PASSWORD"'/g' mongo.js

    mongo --host $MONGO_HOST --port $MONGO_PORT --username=$MONGO_USER --password=MONGO_PASS MONGO_DATABASE mongo.js 

    echo "O usuário criado é $NEW_USER"
    echo "Sua senha cifrada é $NEW_PASS_PROTECTED"
}
