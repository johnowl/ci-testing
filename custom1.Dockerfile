FROM ubuntu:20.04

RUN apt-get update && apt-get install -y git python3.8 python3-pip mongodb-clients
RUN pip3 install --upgrade python-gitlab cookiecutter